// 1. Render songs
// 2. scroll top
// 3. play/pause/seek
// 4. CD rotate
// 5. next/prve
// 6. random
// 7. Next  / Repeat when ended
// 8. Active songs
// 9. Scrool active song into
// 10. Play song when click

/**
 Nhiệm vụ còn lại dành cho các bạn:
1. Hạn chế tối đa các bài hát bị lặp lại
2. Fix lỗi khi tua bài hát, click giữ một chút sẽ thấy lỗi, vì event updatetime nó liên tục chạy dẫn tới lỗi
3. Fix lỗi khi next tới 1-3 bài đầu danh sách thì không “scroll into view”
4. Lưu lại vị trí bài hát đang nghe, F5 lại ứng dụng không bị quay trở về bài đầu tiên
5. Thêm chức năng điều chỉnh âm lượng, lưu vị trí âm lượng người dùng đã chọn. Mặc định 100%
 */

const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const PLAYER_STORAGE_KEY = "F8-PLAYER";

const player = $(".player");
const heading = $("header h2");
const cdThumb = $(".cd-thumb");
const audio = $("#audio");
const cd = $(".cd");
const playBtn = $(".btn-toggle-play");
const progress = $("#progress");
const nextBtn = $(".btn-next");
const prevBtn = $(".btn-prev");
const repeatBtn = $(".btn-repeat");
const randomBtn = $(".btn-random");
const volume = $(".volume-slider");
const iconVolume = $(".volume-icon");

const app = {
  currentIndex: 0,
  isPlaying: false,
  isRepeat: false,
  isRandom: false,
  volumeValue: 1,
  // USE LOCAL STORAGE
  config: JSON.parse(localStorage.getItem(PLAYER_STORAGE_KEY)) || {},
  songs: [
    {
      name: "Vicetone",
      singer: "Nevada",
      path: "music/TheFatRat.mp3",
      image: "https://data.chiasenhac.com/data/cover/85/84521.jpg",
    },
    {
      name: "Anh Sao Va Bau Troi",
      singer: "T.R.I",
      path: "music/AnhSaoVaBauTroi.mp3",
      image:
        "https://avatar-ex-swe.nixcdn.com/mv/2021/09/09/b/8/d/9/1631156018348_640.jpg",
    },
    {
      name: "De Vuong",
      singer: "Dinh Dung",
      path: "music/DeVuong.mp3",
      image: "https://i.ytimg.com/vi/qkPgUgkQE4Y/maxresdefault.jpg",
    },
    {
      name: "Mot Cu Lua",
      singer: "Bich Phuong",
      path: "music/MotCuLua.mp3",
      image: "https://i.ytimg.com/vi/gVlYxmdbYqM/maxresdefault.jpg",
    },
    {
      name: "K-391",
      singer: "Summertime",
      path: "music/K391.mp3",
      image: "https://i1.sndcdn.com/artworks-000089279811-z7mg8j-t500x500.jpg",
    },
    {
      name: "TheFatRat",
      singer: "Anjulie",
      path: "music/TheFatRat.mp3",
      image: "https://i.ytimg.com/vi/gHgv19ip-0c/maxresdefault.jpg",
    },
  ],
  setConfig: function (key, value) {
    this.config[key] = value;
    localStorage.setItem(PLAYER_STORAGE_KEY, JSON.stringify(this.config));
  },
  // load list
  render: function () {
    const _this = this;
    const htmls = this.songs.map(function (song) {
      return `
          <div class="song">
          <div class="thumb" style="background-image: url('${song.image}')">
          </div>
          <div class="body">
            <h3 class="title">${song.name}</h3>
            <p class="author">${song.singer}</p>
          </div>
          <div class="option">
            <i class="fas fa-ellipsis-h"></i>
          </div>
        </div>`;
    });
    $(".playlist").innerHTML = htmls.join("");
    $$(".song").forEach(function (song, index) {
      song.addEventListener("click", function (e) {
        if (!e.target.closest(".option")) {
          if (index !== _this.currentIndex) {
            _this.currentIndex = index;
            _this.loadCurrentSong();
            _this.setSongActive(index);
            _this.setConfig("song", _this.currentIndex);
            audio.play();
          }
        }
      });
    });
  },
  defineProperties: function () {
    Object.defineProperty(this, "currentSong", {
      get: function () {
        return this.songs[this.currentIndex];
      },
    });
  },
  handleEvents: function () {
    const _this = this;
    const cdWidth = cd.offsetWidth;
    // Xu ly CD quay / dung
    const cdThumbAnimate = cdThumb.animate(
      [
        {
          transform: "rotate(360deg)",
        },
      ],
      {
        duration: 10000,
        iterations: Infinity,
      }
    );

    cdThumbAnimate.pause();
    // Xu ly phong to / thu nho CD
    document.onscroll = function () {
      const scrollTop = window.scrollY || document.documentElement.scrollTop;
      const newCdWidth = cdWidth - scrollTop;
      cd.style.width = newCdWidth > 0 ? newCdWidth + "px" : 0;
      cd.style.opacity = newCdWidth / cdWidth;
    };

    // Xu ly khi click play
    playBtn.onclick = function () {
      if (_this.isPlaying) {
        audio.pause();
      } else {
        audio.play();
      }
    };

    // Khi song duoc play
    audio.onplay = function () {
      _this.isPlaying = true;
      player.classList.add("playing");
      cdThumbAnimate.play();
    };

    // Khi song duoc pause
    audio.onpause = function () {
      _this.isPlaying = false;
      player.classList.remove("playing");
      cdThumbAnimate.pause();
    };

    // Xu ly thanh progress
    audio.ontimeupdate = function () {
      if (audio.duration) {
        const progressValue = Math.floor(
          (audio.currentTime / audio.duration) * 100
        );
        progress.value = progressValue;
        _this.setConfig("progress", progressValue);
        _this.setConfig("audioCurrentTime", audio.currentTime);
      }
    };
    // Xu ly repeat
    audio.onended = function () {
      if (_this.isRepeat) {
        audio.play();
      } else if (_this.isRandom) {
        songRandom();
        this.loadCurrentSong();
        audio.play();
      } else {
        _this.nextSong();
        audio.play();
      }
    };

    // Xu li khi click progress
    progress.onchange = function (e) {
      const seekTime = (audio.duration / 100) * e.target.value;
      audio.currentTime = seekTime;
    };

    volume.onchange = function (e) {
      const currentIcon = iconVolume.classList[2];
      audio.volume = e.target.value;
      if (e.target.value > 0.5) {
        iconVolume.classList.replace(currentIcon, "fa-volume-up");
      } else if (e.target.value > 0 && e.target.value <= 0.5) {
        iconVolume.classList.replace(currentIcon, "fa-volume-down");
      } else {
        iconVolume.classList.replace(currentIcon, "fa-volume-mute");
      }
      _this.volumeValue = e.target.value;
      _this.setConfig("volume", _this.volumeValue);
    };

    iconVolume.onclick = function (e) {
      const currentIcon = e.target.classList[2];
      if (audio.volume > 0) {
        iconVolume.classList.replace(currentIcon, "fa-volume-mute");
        audio.volume = 0;
        volume.value = 0;
      } else {
        audio.volume = _this.volumeValue;
        volume.value = _this.volumeValue;
        if (_this.volumeValue > 0.5) {
          iconVolume.classList.replace(currentIcon, "fa-volume-up");
        } else {
          iconVolume.classList.replace(currentIcon, "fa-volume-down");
        }
      }
    };

    // Khi next song
    nextBtn.onclick = function (e) {
      _this.nextSong();
      audio.play();
      _this.scrollToActiveSong();
    };
    // Khi prev song
    prevBtn.onclick = function (e) {
      _this.prevSong();
      audio.play();
      _this.scrollToActiveSong();
    };

    // xu ly repeat song
    repeatBtn.onclick = function (e) {
      _this.repeatSong();
    };

    // Xu ly random song
    randomBtn.onclick = function (e) {
      _this.randomSong();
    };
  },

  loadCurrentSong: function () {
    heading.textContent = this.currentSong.name;
    cdThumb.style.backgroundImage = "url(" + this.currentSong.image + ")";
    audio.src = this.currentSong.path;
    this.setSongActive(this.currentIndex);
    this.scrollToActiveSong();
  },
  loadConfig: function () {
    this.isRandom = this.config.isRandom;
    this.isRepeat = this.config.isRepeat;

    this.currentIndex = this.config.song ? this.config.song : 0;
    progress.value = this.config.progress ? this.config.progress : 0;
    audio.currentTime = this.config.audioCurrentTime
      ? this.config.audioCurrentTime
      : 0;
    this.scrollToActiveSong();
  },

  nextSong: function () {
    this.currentIndex++;
    if (this.isRandom) {
      this.songRandom();
    } else if (this.currentIndex >= this.songs.length) {
      this.currentIndex = 0;
    }
    this.setConfig("song", this.currentIndex);
    this.loadCurrentSong();
  },

  prevSong: function () {
    this.currentIndex--;
    if (this.isRandom) {
      this.songRandom();
    } else if (this.currentIndex < 0) {
      this.currentIndex = this.songs.length - 1;
    }
    this.loadCurrentSong();
    this.setConfig("song", this.currentIndex);
  },
  repeatSong: function () {
    this.isRepeat = !this.isRepeat;
    repeatBtn.classList.toggle("active");
    this.setConfig("isRepeat", this.isRepeat);
  },
  randomSong: function () {
    this.isRandom = !this.isRandom;
    randomBtn.classList.toggle("active");
    this.setConfig("isRandom", this.isRandom);
  },
  songRandom: function () {
    do {
      var indexRandom = Math.floor(Math.random() * this.songs.length - 1) + 1;
    } while (this.currentIndex === indexRandom);
    this.currentIndex = indexRandom;
  },
  setSongActive: function (indexSong) {
    const songsNode = $$(".song");
    const oldSongActive = $(".song.active");
    if (oldSongActive) {
      oldSongActive.classList.remove("active");
    }
    songsNode[indexSong].classList.add("active");
  },
  scrollToActiveSong: function () {
    setTimeout(() => {
      if (this.currentIndex > 3) {
        $(".song.active").scrollIntoView({
          behavior: "smooth",
          block: "end",
        });
      } else {
        $(".song.active").scrollIntoView(alignToTop);
      }
    }, 150);
  },
  start: function () {
    // Dinh nghia cac thuoc tinh cho Object
    this.defineProperties();

    // Lang nghe va xu ly cac su kien (DOM Event)
    this.handleEvents();
    // Render playlist
    this.render();
    // Tai thong tin bai hat dau tien vao UI
    this.loadConfig();
    this.loadCurrentSong();

    // Hien thi trang thai ban dau cua repeat va random
    repeatBtn.classList.toggle("active", this.isRepeat);
    randomBtn.classList.toggle("active", this.isRandom);
  },
};

app.start();

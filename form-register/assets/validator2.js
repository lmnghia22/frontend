function Validator(formSelector, options = {}) {
  function getParent(element, selector) {
    while (element.parentElement) {
      if (element.parentElement.matches(selector)) {
        return element.parentElement;
      } else {
        element = element.parentElement;
      }
    }
  }

  var formRules = [];

  /**
   * Quy uoc tao rule:
   * - Neu co loi thi return `errorMessage`
   * - Neu khong co loi thi return `undefined`
   */
  var validatorRules = {
    required: function (value) {
      return value ? undefined : "Vui lòng nhập trường này";
    },
    email: function (value) {
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return regex.test(value)
        ? undefined
        : "Vui lòng nhập trường này phải là email.";
    },
    min: function (min) {
      return function (value) {
        return value.length >= min
          ? undefined
          : `Vui lòng nhập tối thiểu ${min} kí tự`;
      };
    },
    max: function (max) {
      return function (value) {
        return value.length <= max
          ? undefined
          : `Vui lòng nhập is hon ${max} kí tự`;
      };
    },
  };

  // Lay ra form element trong DOM theo `formSelector`
  var formElement = document.querySelector(formSelector);

  // chi xu ly khi co lement trong DOM
  if (formElement) {
    var inputs = formElement.querySelectorAll("[name][rules]");
    for (var input of inputs) {
      var rules = input.getAttribute("rules").split("|");
      for (var rule of rules) {
        var ruleInfo;
        var isRulehasValue = rule.includes(":");

        if (isRulehasValue) {
          ruleInfo = rule.split(":");
          rule = ruleInfo[0];
        }

        var ruleFunc = validatorRules[rule];

        if (isRulehasValue) {
          ruleFunc = ruleFunc(ruleInfo[1]);
        }

        if (Array.isArray(formRules[input.name])) {
          formRules[input.name].push(ruleFunc);
        } else {
          formRules[input.name] = [ruleFunc];
        }
      }

      // Lang nghe su kien de validate
      input.onblur = handleValidate;
    }
    formElement.onsubmit = function (event) {
      event.preventDefault();
      var inputs = formElement.querySelectorAll("[name][rules]");
      var isValid = true;

      for (var input of inputs) {
        if (!!handleValidate({ target: input })) {
          isValid = false;
        }
      }

      if (isValid) {
        if (typeof options.onSubmit === "function") {
          var enableInputs = formElement.querySelectorAll(
            "[name]:not([disabled])"
          );
          var formValues = Array.from(enableInputs).reduce(function (
            values,
            input
          ) {
            switch (input.type) {
              case "file":
                values[input.name] = input.files;
                break;
              case "radio":
                var elementRadio = formElement.querySelector(
                  'input[name="' + input.name + '"]:checked'
                );
                values[input.name] = elementRadio ? elementRadio.value : "";
                break;
              case "checkbox":
                if (!input.matches(":checked")) {
                  values[input.name] = "";
                  return values;
                }
                if (!Array.isArray(values[input.name])) {
                  values[input.name] = [];
                }
                values[input.name].push(input.value);
                break;
              default:
                values[input.name] = input.value;
            }
            return values;
          },
          {});
          options.onSubmit(formValues);
          return;
        }
        formElement.onsubmit();
      }
    };
  }

  function handleValidate(event) {
    var rules = formRules[event.target.name];
    var errorMessage;
    rules.find(function (rule) {
      errorMessage = rule(event.target.value);
      return;
    });
    var elementParent = getParent(event.target, ".form-group");
    if (errorMessage) {
      elementParent.querySelector(".form-message").innerText = errorMessage;
      elementParent.classList.add("invalid");
    } else {
      elementParent.querySelector(".form-message").innerText = "";
      elementParent.classList.remove("invalid");
    }
    return errorMessage;
  }
}

var nodeTab = document.querySelectorAll(".tab-item");
var tabPane = document.querySelectorAll(".tab-pane");

const line = document.querySelector(".tabs .line");
var tabActive = document.querySelector(".tab-item.active");

line.style.left = tabActive.offsetLeft + "px";
line.style.width = tabActive.offsetWidth + "px";

nodeTab.forEach((item, index) => {
  item.onclick = function () {
    document.querySelector(".tab-item.active").classList.remove("active");
    document.querySelector(".tab-pane.active").classList.remove("active");
    tabActive = this;
    this.classList.add("active");
    tabPane[index].classList.add("active");

    line.style.left = tabActive.offsetLeft + "px";
    line.style.width = tabActive.offsetWidth + "px";
  };
});

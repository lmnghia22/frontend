const courseApi = "http://localhost:3000/courses";

function start() {
  getCourses(renderCourses);

  handleCreateCourseForm();
  document.getElementById("delete").addEventListener("click", function (event) {
    event.preventDefault();
  });
}

start();

function getCourses(callback) {
  fetch(courseApi)
    .then(function (reponse) {
      return reponse.json();
    })
    .then(callback);
}

function getCourseById(id, callback) {
  fetch(courseApi + "/" + id)
    .then(function (reponse) {
      return reponse.json();
    })
    .then(callback);
}

function createCourse(data, callback) {
  var options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(data),
  };

  fetch(courseApi, options)
    .then(function (reponse) {
      reponse.json();
    })
    .then(callback);
}

function editCourse(id, data, callback) {
  var options = {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(data),
  };

  fetch(courseApi + "/" + id, options)
    .then(function (reponse) {
      reponse.json();
    })
    .then(callback);
}

function deleteCourse(id) {
  var options = {
    method: "DELETE",
  };
  fetch(courseApi.concat("/" + id), options)
    .then(function (reponse) {
      reponse.json();
    })
    .then(function () {
      document.querySelector(".course-item-" + id).remove();
    });
}

function renderCourses(courses) {
  const listCoursesBlock = document.querySelector("#list-courses");

  const htmls = courses.map(function (course) {
    return `
       <li class="course-item-${course.id}">
       <h4>${course.name}</h4>
       <img src="${course.image}" alt="${course.name}">
       <p>${course.description}</p>
       <button style="color: red" id="delete" onclick="deleteCourse(${course.id})">Delete</button>
       <button style="yellow: red" id="edit" onclick="handleEditCourse(${course.id})">Edit</button>
        </li>`;
  });
  listCoursesBlock.innerHTML = htmls.join("");
}

function handleCreateCourseForm() {
  const buttonCreate = document.getElementById("create");
  buttonCreate.onclick = function () {
    const name = document.querySelector('input[name="name"]').value;
    const description = document.querySelector(
      'input[name="description"]'
    ).value;
    var fromData = {
      name: name,
      description: description,
    };
    createCourse(fromData, function () {
      getCourses(renderCourses);
    });
  };
}

function handleEditCourseForm(course) {
  const title = document.querySelector(".title");
  const btnEdit = document.getElementById("create");
  btnEdit.scrollIntoView({
    behavior: "smooth",
    block: "center",
  });
  btnEdit.innerText = "Edit";
  title.innerHTML = "Chinh sua khoa hoc";
  const name = (document.querySelector('input[name="name"]').value =
    course.name);
  const description = (document.querySelector(
    'input[name="description"]'
  ).value = course.description);

  btnEdit.onclick = function () {
    const name = document.querySelector('input[name="name"]').value;
    const description = document.querySelector(
      'input[name="description"]'
    ).value;
    var fromData = {
      name: name,
      description: description,
    };
    console.log(fromData);
    editCourse(course.id, fromData, function () {
      getCourses(renderCourses);
    });
  };
}

function handleEditCourse(id) {
  getCourseById(id, function (data) {
    handleEditCourseForm(data);
  });
}

function toast(title = "", message = "", type = "info", duration = 3000) {
  var main = document.getElementById("toast");
  if (
    type === "success" ||
    type === "warning" ||
    type === "error" ||
    type === "info"
  ) {
    const toast = document.createElement("div");
    toast.classList.add("toast", `toast__${type}`);
    // auto remove toast
    const autoRemoveId = setTimeout(() => {
      main.removeChild(toast);
    }, duration);

    // remove toast when clicked
    toast.onclick = function (e) {
      if (e.target.closest(".toast__close")) {
        clearTimeout(autoRemoveId);
        main.removeChild(toast);
      }
    };

    const delay = (duration / 1000).toFixed(2);
    toast.style.animation = `animation: slideInLeft ease 0.3s, fadeOut linear 1s ${delay} forwards;`;

    toast.innerHTML = `
            <div class="toast__icon">
                <i class="fas fa-check-circle"></i>
            </div>
            <div class="toast__body">
                <h3 class="toast__title">${title}</h3>
                <p class="toast__msg">${message}</p>
            </div>
            <div class="toast__close">
                <i class="fas fa-times"></i>
            </div>
        </div>`;
    main.appendChild(toast);
  }
}

function showToastSuccess() {
  toast("Success", "Ban da dang ky thanh cong roi", "success", 3000);
}

function showToastError() {
  toast("Error", "Ban da dang ky thanh cong roi", "error");
}

function showToastWarning() {
  toast("Warning", "Ban da dang ky thanh cong roi", "warning");
}

function showToastInfo() {
  toast("Info", "Ban da dang ky thanh cong roi", "info");
}
